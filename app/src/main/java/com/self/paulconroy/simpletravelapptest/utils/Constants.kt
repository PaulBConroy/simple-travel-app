package com.self.paulconroy.simpletravelapptest.utils

/**
 * A Singleton Constants class used to house the constant values used by the application.
 */

class Constants {

    companion object {
        val instance = Constants()
    }

    /**
     * String constant of the URL used to fetch the data.
     */
    val BASE_URL = "http://api.irishrail.ie/realtime/realtime.asmx/"

    /**
     * String constant of the time format used for time conversion.
     */
    val TIME_DATE_FORMAT = "HH:mm"

    /**
     * Magic Number
     * Integer constant of number of minutes used in grace period.
     */
    val THREE_MINUTE = 3

    /**
     * Magic Number
     * Integer constant of number of minutes used in grace period.
     */
    val TWENTY_FOUR_MINUTE = 24

    /**
     * Magic Number
     * Integer constant of number value of a second.
     */
    val ONE_SECOND = 1000

    /**
     * Magin Number
     * Integer constant of number value of 60.
     */
    val SIXTY = 60


}