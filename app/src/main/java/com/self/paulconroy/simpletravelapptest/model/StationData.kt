package com.self.paulconroy.simpletravelapptest.model

class StationData {
    var direction = ""
    var destinationTo = ""
    var destinationFrom = ""
    val trainList = mutableListOf<ObjStationData>()
}