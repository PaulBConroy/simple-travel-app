package com.self.paulconroy.simpletravelapptest.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "objStationData", strict = false)
class ObjStationData {

    @field:Element(name = "Stationfullname", required = false)
    var stationName: String? = null

    @field:Element(name = "Stationcode", required = false)
    var stationCode: String? = null

    @field:Element(name = "Exparrival", required = false)
    var expectedArrival: String? = null

    @field:Element(name = "Destination", required = false)
    var destination: String? = null

    @field:Element(name = "Destinationtime", required = false)
    var destinationTime: String? = null

    @field:Element(name = "Direction", required = false)
    var direction: String? = null

    @field:Element(name = "Lastlocation", required = false)
    var lastLocation: String? = null

    @field:Element(name = "Duein", required = false)
    var dueIn: String? = null

    @field:Element(name = "Traincode", required = false)
    var trainCode: String? = null

    @field:Element(name = "Expdepart", required = false)
    var expDepart: String? = null

}