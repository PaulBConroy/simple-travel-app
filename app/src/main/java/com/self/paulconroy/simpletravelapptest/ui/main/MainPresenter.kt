package com.self.paulconroy.simpletravelapptest.ui.main

/**
 * The presenter is used to invoke logic methods from the activity
 *
 */
interface MainPresenter {

    /**
     * Begin to gather data with chain api calls.
     */
    fun beginToGatherStationData()
}