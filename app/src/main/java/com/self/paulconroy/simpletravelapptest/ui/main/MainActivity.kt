package com.self.paulconroy.simpletravelapptest.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import com.self.paulconroy.cartrawlertest.adapter.TrainListAdapter
import com.self.paulconroy.cartrawlertest.custom.NoConnectionDialog
import com.self.paulconroy.simpletravelapptest.R
import com.self.paulconroy.simpletravelapptest.model.StationData

class MainActivity : AppCompatActivity(), MainView {

    private var presenter: MainPresenter? = null
    lateinit var rvAvailableTrainList: RecyclerView
    lateinit var pbProgress: ProgressBar
    private var noConnectionDialog: NoConnectionDialog? = null
    private var availableTrainAdapter: TrainListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvAvailableTrainList = findViewById(R.id.rvAvailableTrainList)
        pbProgress = findViewById(R.id.pbProgress)
    }

    override fun onResume() {
        super.onResume()
        getPresenter().beginToGatherStationData()
    }

    /**
     * Returns a single instance of the Main Presenter class
     */
    fun getPresenter(): MainPresenterImp {
        if (presenter == null) {
            presenter = MainPresenterImp(this, this)
        }
        return presenter as MainPresenterImp
    }

    override fun displayNoConnectionDialog() {
        noConnectionDialog = NoConnectionDialog(this)
        noConnectionDialog!!.showDialog()
    }

    override fun updateTrainList(stationDataList: List<StationData>) {
        if (availableTrainAdapter == null) {
            availableTrainAdapter = TrainListAdapter(this, stationDataList)
            rvAvailableTrainList.layoutManager = LinearLayoutManager(this)
            rvAvailableTrainList.adapter = availableTrainAdapter
            availableTrainAdapter!!.notifyDataSetChanged()
            rvAvailableTrainList.invalidate()
        }
    }

    override fun hideProgressBar() {
        pbProgress.visibility = View.GONE
    }
}
