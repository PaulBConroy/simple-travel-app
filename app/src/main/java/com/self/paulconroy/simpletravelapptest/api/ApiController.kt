package com.self.paulconroy.simpletravelapptest.api

import com.self.paulconroy.simpletravelapptest.model.ArrayOfObjStationData
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The controller is used to host the methods used for Retrofit to retrieve Station information.
 */
interface ApiController {

    /**
     * Fetch the results for the Arklow data.
     */
    @GET("getStationDataByNameXML?StationDesc=Arklow")
    fun getArklowStationData(): Observable<ArrayOfObjStationData>

    /**
     * Fetch the results for the Shankill data.
     */
    @GET("getStationDataByNameXML?StationDesc=Shankill")
    fun getShankillStationData(): Observable<ArrayOfObjStationData>

    /**
     * Fetch the results for the Bray data.
     */
    @GET("getStationDataByNameXML?StationDesc=Bray")
    fun getBrayStationData(): Observable<ArrayOfObjStationData>
}