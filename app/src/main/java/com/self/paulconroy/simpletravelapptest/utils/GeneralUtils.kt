package com.self.paulconroy.simpletravelapptest.utils

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.util.*

/**
 * A Singleton General Utility class for time conversions.
 */
class GeneralUtils {

    companion object {
        val instance = GeneralUtils()
    }

    /**
     * Function used to calculate the time (in minutes) between the departure and arrival of two
     * trains.
     *
     * @param expectedDeparture the expected departure time string
     * @param expectedArrival the expected arrival time string
     *
     * @return Integer value of time (in minutes).
     */
    fun convertTimeString(expectedDeparture: String, expectedArrival: String): Int {

        val dateFormat = DateTimeFormat.forPattern(Constants().TIME_DATE_FORMAT)
        val dateTime1 = DateTime(dateFormat.parseDateTime(expectedDeparture), DateTimeZone.getDefault())
        val dateTime2 = DateTime(dateFormat.parseDateTime(expectedArrival), DateTimeZone.getDefault())
        val cal1 = dateTime1.toCalendar(Locale.getDefault())
        val cal2 = dateTime2.toCalendar(Locale.getDefault())
        val difference = ((cal1.timeInMillis - cal2.timeInMillis)
                / Constants.instance.ONE_SECOND) / Constants.instance.SIXTY

        return difference.toInt()
    }
}