package com.self.paulconroy.simpletravelapptest.api

import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.self.paulconroy.simpletravelapptest.model.ArrayOfObjStationData
import com.self.paulconroy.simpletravelapptest.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

/**
 * API Client used to set the controller and invoke controller methods.
 */
class ApiClient : Callback<ArrayOfObjStationData> {

    lateinit var results: ArrayOfObjStationData
    lateinit var apiController: ApiController

    override fun onFailure(call: Call<ArrayOfObjStationData>, t: Throwable) {
        Log.e("ApiClient: Error ", t.message)
    }

    override fun onResponse(call: Call<ArrayOfObjStationData>, response: Response<ArrayOfObjStationData>) {
        results = response.body()!!
    }

    init {
        start()
    }

    /**
     * Constructs the api controller
     */
    fun start() {

        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(getBaseURL())
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        apiController = retrofit.create(ApiController::class.java)
    }

    /**
     * The base URL string to retrieve the station data.
     */
    private fun getBaseURL(): String {
        return Constants.instance.BASE_URL
    }
}