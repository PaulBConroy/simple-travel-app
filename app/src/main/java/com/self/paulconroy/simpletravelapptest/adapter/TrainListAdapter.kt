package com.self.paulconroy.cartrawlertest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.self.paulconroy.cartrawlertest.custom.InnerTrainCard
import com.self.paulconroy.simpletravelapptest.R
import com.self.paulconroy.simpletravelapptest.model.ObjStationData
import com.self.paulconroy.simpletravelapptest.model.StationData
import com.self.paulconroy.simpletravelapptest.ui.main.MainActivity

/**
 * TrainListAdapter Recyclerview will hold and recycle itemviews containing available trains for
 * each direction. Each item  will dynamically add the number of trains.
 */
class TrainListAdapter(private val activity: MainActivity,
                       private var availableTrainList: List<StationData>): RecyclerView.Adapter<TrainListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainListAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.direction_container_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return availableTrainList.size
    }

    override fun onBindViewHolder(holder: TrainListAdapter.ViewHolder, position: Int) {
        var hasChange = false

        holder.tvDirection.text = this.availableTrainList[position].direction
        holder.tvExitText.text = activity.resources.getString(R.string.exit_text,
                this.availableTrainList[position].destinationTo)
        holder.tvDirectionLabel.text = activity.resources.getString(R.string.from_text,
                this.availableTrainList[position].destinationFrom)

        if (this.availableTrainList[position].trainList.isEmpty()) {
            holder.llDirectionContainer.addView(getCustomEmptyInnerTrainCard())
        }

        for (trainAvails in this.availableTrainList[position].trainList) {
            holder.llDirectionContainer.addView(getCustomInnerTrainCard(trainAvails))
            if (!hasChange && availableTrainList.isNotEmpty()) {
                hasChange = true
                holder.llDirectionContainer.addView(getCustomChangeInnerTrainCard())
            }
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvDirection: TextView = itemView.findViewById(R.id.tvDirection)
        val llDirectionContainer: LinearLayout = itemView.findViewById(R.id.llDirectionContainer)
        val tvDirectionLabel: TextView = itemView.findViewById(R.id.tvDirectionLabel)
        val tvExitText: TextView = itemView.findViewById(R.id.tvExitText)
    }

    /**
     *  Prepare and return a custom inner train card to be dynamically added to the item view.
     *
     *  @param objStationData the information to be added to the inner custom station card.
     *  @return Returns a custom InnerTrainCard that was constructed.
     */
    private fun getCustomInnerTrainCard(objStationData: ObjStationData): InnerTrainCard {
        val innerCardCard = InnerTrainCard(activity)
        innerCardCard.rlNoTrains.visibility = View.GONE
        innerCardCard.rlChangeBray.visibility = View.GONE
        innerCardCard.tvExpectedArrival.text = activity.resources.getString(R.string.arrival_string,
                objStationData!!.expectedArrival)
        innerCardCard.tvDueIn.text = objStationData.dueIn + "mins"
        innerCardCard.tvDestination.text = activity.resources.getString(R.string.travel_to,
                objStationData.destination)
        return innerCardCard
    }

    /**
     *  Prepare and return an empty custom inner train card to be dynamically added to the item view.
     *
     *  @return Returns an empty custom InnerTrainCard that was constructed.
     */
    private fun getCustomEmptyInnerTrainCard(): InnerTrainCard {
        val innerTrainCard = InnerTrainCard(activity)
        innerTrainCard.rlNoTrains.visibility = View.VISIBLE
        return innerTrainCard
    }

    /**
     *  Prepare and return an custom change inner train card to be dynamically added to the item view.
     *
     *  @return Returns a custom change InnerTrainCard that was constructed.
     */
    private fun getCustomChangeInnerTrainCard(): InnerTrainCard {
        val innerTrainCard = InnerTrainCard(activity)
        innerTrainCard.rlChangeBray.visibility = View.VISIBLE
        return innerTrainCard
    }

}