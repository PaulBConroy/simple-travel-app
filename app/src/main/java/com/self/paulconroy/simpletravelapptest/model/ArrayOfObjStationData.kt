package com.self.paulconroy.simpletravelapptest.model

import org.simpleframework.xml.Root
import org.simpleframework.xml.ElementList


@Root(name = "ArrayOfObjStationData", strict = false)
class ArrayOfObjStationData {

    @field:ElementList(inline = true, required = false)
    var obStationDataList: List<ObjStationData>? = null

}