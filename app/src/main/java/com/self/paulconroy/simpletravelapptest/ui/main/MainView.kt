package com.self.paulconroy.simpletravelapptest.ui.main

import com.self.paulconroy.simpletravelapptest.model.StationData

/**
 * The view allows the presenter implementation to interact with the activity UI.
 */
interface MainView {

    /**
     * Update the UI with the station data.
     */
    fun updateTrainList(stationDataList: List<StationData>)

    /**
     * If there is no network connection, display No Connection Dialog to user.
     */
    fun displayNoConnectionDialog()

    /**
     * Hides the Progress Bar on the UI View
     */
    fun hideProgressBar()
}