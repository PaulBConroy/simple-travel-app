package com.self.paulconroy.simpletravelapptest.ui.main

import android.content.Context
import android.util.Log
import com.self.paulconroy.simpletravelapptest.R
import com.self.paulconroy.simpletravelapptest.api.ApiClient
import com.self.paulconroy.simpletravelapptest.model.ArrayOfObjStationData
import com.self.paulconroy.simpletravelapptest.model.DirectionStationTrainList
import com.self.paulconroy.simpletravelapptest.model.ObjStationData
import com.self.paulconroy.simpletravelapptest.model.StationData
import com.self.paulconroy.simpletravelapptest.utils.Constants
import com.self.paulconroy.simpletravelapptest.utils.GeneralUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.disposables.CompositeDisposable

/**
 * The presenter implementation implements logic methods. No UI methods are to be placed here,
 * only logic, business, api calls etc.
 */
class MainPresenterImp(val context: Context, val view: MainView) : MainPresenter {

    private val southBoundBrayTempList = mutableListOf<ObjStationData>()
    private val southBoundShankillTempList = mutableListOf<ObjStationData>()
    private val southBoundArklowTempList = mutableListOf<ObjStationData>()

    private val northBoundArklowTempList = mutableListOf<ObjStationData>()
    private val northBoundBrayTempList = mutableListOf<ObjStationData>()
    private val northBoundShankillTempList = mutableListOf<ObjStationData>()

    private var directionStationTrainList = DirectionStationTrainList()


    /**
     * An API call to gather data from Shankill station
     */
    private fun getShankillStationData() {
        southBoundShankillTempList.clear()
        northBoundShankillTempList.clear()
        val compositeDisposable = CompositeDisposable()
        val disposableObserver = ApiClient().apiController.getShankillStationData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ArrayOfObjStationData>() {
                    override fun onComplete() {
                        Log.e("========== onComplete ", "Complete")
                        getBrayStationData()
                        compositeDisposable.dispose()
                    }

                    override fun onNext(t: ArrayOfObjStationData) {
                        Log.d("========== onNext ", "Station Data received")
                        if (t.obStationDataList != null)
                            handleStationData(t, context.resources.getString(R.string.shankill_string))
                    }

                    override fun onError(e: Throwable) {
                        Log.e("========== onError ", e.message)
                        view.displayNoConnectionDialog()
                        compositeDisposable.dispose()
                    }

                })

        compositeDisposable.add(disposableObserver)
    }

    /**
     * An API call to gather data from Bray station
     */
    private fun getBrayStationData() {
        southBoundBrayTempList.clear()
        northBoundBrayTempList.clear()
        val compositeDisposable = CompositeDisposable()
        val disposableObserver = ApiClient().apiController.getBrayStationData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ArrayOfObjStationData>() {
                    override fun onComplete() {
                        Log.e("========== onComplete ", "Complete")
                        getArklowStationData()
                        compositeDisposable.dispose()
                    }

                    override fun onNext(t: ArrayOfObjStationData) {
                        Log.d("========== onNext ", "Station Data received")
                        if (t.obStationDataList != null)
                            handleStationData(t, context.resources.getString(R.string.bray_string))
                    }

                    override fun onError(e: Throwable) {
                        Log.e("========== onError ", e.message)
                        view.displayNoConnectionDialog()
                        compositeDisposable.dispose()
                    }

                })
        compositeDisposable.add(disposableObserver)
    }

    /**
     * An API call to gather data from Arklow station
     */
    private fun getArklowStationData() {
        southBoundArklowTempList.clear()
        northBoundArklowTempList.clear()
        val compositeDisposable = CompositeDisposable()
        val disposableObserver = ApiClient().apiController.getArklowStationData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ArrayOfObjStationData>() {
                    override fun onComplete() {
                        Log.e("========== onComplete ", "Complete")
                        stationLogic()
                        compositeDisposable.dispose()
                    }

                    override fun onNext(t: ArrayOfObjStationData) {
                        Log.d("========== onNext ", "Station Data received")
                        if (t.obStationDataList != null)
                            handleStationData(t, context.resources.getString(R.string.arklow_string))
                    }

                    override fun onError(e: Throwable) {
                        Log.e("========== onError ", e.message)
                        view.displayNoConnectionDialog()
                        compositeDisposable.dispose()
                    }

                })
        compositeDisposable.add(disposableObserver)
    }

    /**
     * Using the set of data returned from each chain API call, filter the data into particular lists.
     *
     * @param t the data returned from the API xml
     * @param stationString the station this data was gathered from
     */
    private fun handleStationData(t: ArrayOfObjStationData, stationString: String) {
        when (stationString) {
            context.resources.getString(R.string.shankill_string) -> for (data in t.obStationDataList!!) {
                when (data.direction) {
                    context.resources.getString(R.string.southbound) -> southBoundShankillTempList.add(data)
                    context.resources.getString(R.string.northbound) -> northBoundShankillTempList.add(data)
                }
            }
            context.resources.getString(R.string.bray_string) -> for (data in t.obStationDataList!!) {
                when (data.direction) {
                    context.resources.getString(R.string.southbound) -> southBoundBrayTempList.add(data)
                    context.resources.getString(R.string.northbound) -> northBoundBrayTempList.add(data)
                }
            }
            context.resources.getString(R.string.arklow_string) -> for (data in t.obStationDataList!!) {
                when (data.direction) {
                    context.resources.getString(R.string.southbound) -> southBoundArklowTempList.add(data)
                    context.resources.getString(R.string.northbound) -> northBoundArklowTempList.add(data)
                }
            }

        }
    }

    /**
     * The stationLogic function calculates the route the user should take to reach their
     * destination both Southbound and Northbound.
     *
     * Both Shankill and Arklow share a common station in which the user MUST stop at :: Bray.
     * Each station's data is queried and once a train has been found travelling in a given
     * direction, the train's traincode is used to determine when it should arrive in Bray, once
     * that has been found then the Bray station's data is queried to find a train that departs
     * 3 -> 25 minute from that station towards the user's destination.
     *
     * 3 -> 25 minute grace period was chosen as it seem to vary between this two magic numbers on
     * the Irish Rail Realtime website.
     *
     * [PLEASE NOTE THAT STATIONS ONLY DISPLAY TRAINS UP TO 90 MINUTES BEFORE THEY ARRIVE AT THAT
     * PARTICULAR STATION]
     */
    fun stationLogic() {
        directionStationTrainList.stationList.clear()
        //Southbound direction [Shankill -> Bray -> Arklow] is queried.
        if (directionStationTrainList.stationList.isEmpty()) {
            var stationData = StationData()
            stationData.direction = context.resources.getString(R.string.southbound)
            stationData.destinationTo = context.getString(R.string.arklow_string)
            stationData.destinationFrom = context.getString(R.string.shankill_string)
            first@ for (j in southBoundBrayTempList) {
                if (j.destination.equals(context.getString(R.string.rosslare_europort_string))) {
                    for (x in southBoundShankillTempList) {
                        if (GeneralUtils.instance.convertTimeString(j.expDepart!!,
                                        x.expectedArrival!!)
                                in Constants.instance.THREE_MINUTE..Constants.instance.TWENTY_FOUR_MINUTE) {
                            stationData.trainList.add(x)
                            stationData.trainList.add(j)
                            break@first
                        }
                    }
                }
            }
            directionStationTrainList.stationList.add(stationData)
        }

        //Northbound direction [Arklow -> Bray -> Shankill] is queried.
        if (directionStationTrainList.stationList.size == 1) {
            var stationData = StationData()
            stationData.direction = context.resources.getString(R.string.northbound)
            stationData.destinationTo = context.getString(R.string.shankill_string)
            stationData.destinationFrom = context.getString(R.string.arklow_string)
            first@ for (j in northBoundArklowTempList) {
                for (x in northBoundBrayTempList) {
                    if (j.trainCode == x.trainCode) {
                        for (y in northBoundBrayTempList) {
                            if (GeneralUtils.instance.convertTimeString(y.expDepart!!,
                                            x.expectedArrival!!)
                                    in Constants.instance.THREE_MINUTE..Constants.instance.TWENTY_FOUR_MINUTE) {
                                stationData.trainList.add(j)
                                stationData.trainList.add(y)
                                break@first
                            }
                        }
                    }
                }
                //Assume the Bray station has not updated Arklow Northbound train
                //Add Arklow train and inform user to alight at Bray.
                if (stationData.trainList.isEmpty()) {
                    stationData.trainList.add(j)
                }
            }
            directionStationTrainList.stationList.add(stationData)
        }

        //Using the newly created stationTrainList, the UI is manipulated to display the data.
        applyToUi()
    }

    /**
     * Display the constructed data to the user
     */
    private fun applyToUi() {
        view.hideProgressBar()
        view.updateTrainList(directionStationTrainList.stationList)
    }

    override fun beginToGatherStationData() {
        getShankillStationData()
    }
}