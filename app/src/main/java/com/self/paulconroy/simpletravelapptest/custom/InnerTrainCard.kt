package com.self.paulconroy.cartrawlertest.custom

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import android.widget.TextView
import com.self.paulconroy.simpletravelapptest.R

/**
 * Custom Nested Train Card to display information about the train inside the direction Item View
 * on the RecyclerView
 */
class InnerTrainCard: RelativeLayout {

    private var mInflater: LayoutInflater
    lateinit var rlNoTrains: RelativeLayout
    lateinit var rlChangeBray: RelativeLayout
    lateinit var tvDestination: TextView
    lateinit var tvDueIn: TextView
    lateinit var tvExpectedArrival: TextView
    lateinit var cvCardContainer: CardView

    constructor(context: Context) : super(context) {
        mInflater = LayoutInflater.from(context)
        init()

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    private fun init() {
        val v = mInflater.inflate(R.layout.nested_train_item, this, true)
        cvCardContainer = v.findViewById(R.id.cvCardContainer)
        tvDestination = v.findViewById(R.id.tvDestination)
        tvDueIn = v.findViewById(R.id.tvDueIn)
        tvExpectedArrival = v.findViewById(R.id.tvExpectedArrival)
        rlNoTrains = v.findViewById(R.id.rlNoTrains)
        rlChangeBray = v.findViewById(R.id.rlChangeBray)
    }
}